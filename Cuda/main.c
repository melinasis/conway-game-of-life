#include <stdio.h>
#include <stdlib.h>
#include "structs.h"

void evaluateGrid(procInfo *);

void destroy(procInfo *data){
	free(data->grid);
	free(data->nextGrid);
}

void randomizeGrid(procInfo *data){
	int i,j;

	srand(time(NULL));
	for(i = 1; i <= data->rows; i++){
		for(j = 1; j <= data->cols; j++){
			GRID(i,j) = (rand() % 2) ? ALIVE : DEAD;
			//GRID(i,j) = i * (data->cols + 2) + j;
		}
	}
}

int main(int argc, char *argv[]){
	procInfo block, *data;
	char *check;
	char *filename;
	FILE* file;
	int i,j;

  if(argc < 4 ){
    printf("Please execute as: %s <num of Rows> <num of Columns> <generations> {input file}\n", argv[0]);
    return EXIT_FAILURE;
  }

	block.rows = (int)(strtol(argv[1], &check, 10));
  if ((*check != '\0') || (block.rows < 1)){
    printf("Wrong input! Please type a valid number for the rows\n");
    return EXIT_FAILURE;
  }

  block.cols = (int)(strtol(argv[2], &check, 10));
  if ((*check != '\0') || (block.cols < 1)){
    printf("Wrong input! Please type a valid number for the cols\n");
    return EXIT_FAILURE;
  }

  block.gens = (int)(strtol(argv[3], &check, 10));
  if ((*check != '\0') || (block.gens < 1)){
    printf("Wrong input! Please type a valid number for the generations\n");
    return EXIT_FAILURE;
  }

    block.grid = calloc((block.rows + 2) * (block.cols + 2),sizeof(int));
    block.nextGrid = calloc((block.rows +2) * (block.cols + 2),sizeof(int));

	if ((filename = argv[4]) == NULL){
		randomizeGrid(&block);
	}
 	else {
		data = &block;
 		file = fopen(filename,"r");
		while(fscanf(file,"%d %d\n",&i,&j) != EOF){
			if ((i <= block.rows) && (j <= block.cols)){
				GRID(i,j) = ALIVE;
			}
		}
		for(i = 1; i <= block.rows; i++){
			for(j = 1; j <= block.cols; j++){
				printf("%4d", GRID(i,j));
			}
			printf("\n");
		}

 	}

	evaluateGrid(&block);

	return EXIT_SUCCESS;
}
