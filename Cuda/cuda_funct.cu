#include "structs.h"
#include <time.h>
#include <stdlib.h>

#define CU_GRID(x,y) (grid[(x) * (cols + 2) + y])
#define CU_NEXTGRID(x,y) (nextGrid[(x) * (cols + 2) + y])

#define BLOCK_SIZE 8

__global__ void calculateCuda(int *grid, int *nextGrid, int rows, int cols){
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    int j = blockIdx.x * blockDim.x + threadIdx.x;
    int aliveNeighbours;
    if ((i <= rows) && (j <= cols) || (i >= 1) || (j >= 1)){
			aliveNeighbours = CU_GRID(i-1,j-1) + CU_GRID(i,j-1) + CU_GRID(i+1,j-1) + CU_GRID(i+1,j)
											+ CU_GRID(i+1,j+1) + CU_GRID(i,j+1) + CU_GRID(i-1,j+1) + CU_GRID(i-1,j);
			if(CU_GRID(i,j) == ALIVE){
					switch(aliveNeighbours/ALIVE){
							case 2:
							case 3:
									CU_NEXTGRID(i,j) = ALIVE;
									break;
							default:
									CU_NEXTGRID(i,j) = DEAD;
					}
			}
			else{
					if(aliveNeighbours/ALIVE == 3)
							CU_NEXTGRID(i,j) = ALIVE;
					else
							CU_NEXTGRID(i,j) = DEAD;
			}
		}
}


void calculateSerial(procInfo *data){
    int i,j,aliveNeighbours;
    for(i = 1; i <= data->rows; i++){
        for(j = 1; j <= data->cols; j++){
            aliveNeighbours = GRID(i-1,j-1)
                                                + GRID(i,j-1)
                                                + GRID(i+1,j-1)
                                                + GRID(i+1,j)
                                                + GRID(i+1,j+1)
                                                + GRID(i,j+1)
                                                + GRID(i-1,j+1)
                                                + GRID(i-1,j);
            if(GRID(i,j) == ALIVE){
                switch(aliveNeighbours/ALIVE){
                    case 2:
                    case 3:
                        NEXTGRID(i,j) = ALIVE;
                        break;
                    default:
                        NEXTGRID(i,j) = DEAD;
                }
            }
            else{
                if(aliveNeighbours/ALIVE == 3)
                    NEXTGRID(i,j) = ALIVE;
                else
                    NEXTGRID(i,j) = DEAD;
            }
        }
    }
}

void torodizeGrid(procInfo *data){
	int i;
    //wrap edges - make it periodic
    for(i = 1; i <= data->rows; i++)
        GRID(i,data->cols + 1) = GRID(i,1);
    for(i = 1; i <= data->rows; i++)
        GRID(i,0) = GRID(i,data->cols);
    for(i = 1; i <= data->cols; i++)
        GRID(data->rows + 1,i) = GRID(1,i);
    for(i = 1; i <= data->cols; i++)
        GRID(0,i) = GRID(data->rows,i);
    GRID(data->rows+1,data->cols+1) = GRID(1,1);
    GRID(data->rows + 1,0) = GRID(1,data->cols);
    GRID(0,data->cols + 1) = GRID(data->rows,1);
    GRID(0,0) = GRID(data->rows,data->cols);

}

void printGrid(procInfo *data){
    int i,j;
    for(i = 1; i <= data->rows; i++){
        for(j = 1; j <= data->cols; j++)
            printf("%4d", GRID(i,j));
        printf("\n");
    }
}

void cpyGrid(int *dest, procInfo *data){
    int i,j;
    for(i = 1; i <= data->rows; i++){
        for(j = 1; j <= data->cols; j++)
            dest[i * (data->cols + 2) + j] = GRID(i,j);
    }
}

extern "C" void evaluateGrid(procInfo *data){
    FILE * file;
    int i;
    int *cudaGrid, *cudaNextGrid, *swap;
    cudaEvent_t start, stop;
    clock_t cpuStart, cpuStop;
    int size;
    cudaError_t err;
    int *initialGrid;
    float elapsedTime, cpuElapsedTime;

    size = (data->rows + 2) * (data->cols + 2) * sizeof(int);
    initialGrid = (int *) malloc(size);
    cpyGrid(initialGrid, data); //copy it so Cuda has the same matrix to work with after serial is done.
    cpuElapsedTime = 0.0;
	///CUDA ALLOCATION AND INIT
    err = cudaMalloc((void**) &cudaGrid, size);
    if (err) {printf("Error allocating memory on gfx card: %s\n",cudaGetErrorString(err)); return;}
    err = cudaMalloc((void**) &cudaNextGrid, size);
    if (err) {printf("Error allocating memory on gfx card: %s\n",cudaGetErrorString(err)); return;}

    dim3 blockSize(BLOCK_SIZE, BLOCK_SIZE,1);
    dim3 gridSize((data->rows + BLOCK_SIZE + 1)/ BLOCK_SIZE, (data->cols + BLOCK_SIZE + 1)/BLOCK_SIZE, 1); //getting ceiling of division. + 1 is + 2 - 1.
	///END CUDA ALLOCATION AND INIT
    ///SERIAL
    for (i = 0; i < data->gens; i++){
        torodizeGrid(data);
        cpuStart = clock();
        calculateSerial(data);
        cpuStop = clock();
        cpuElapsedTime += (cpuStop - cpuStart) / (float)CLOCKS_PER_SEC;
        swap = data->grid;
        data->grid = data->nextGrid;
        data->nextGrid = swap;
    }
    ///END SERIAL
    ///CUDA
    ///initiating events
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start, 0);
    //recovering initial matrix
    free(data->grid);
    data->grid = initialGrid;
    //torodizeGrid() and swap remain on CPU - this part of the original algorithm is handled by the MPI, so
		//to be "fair" with our estimates the time that is needed to copy the array back and forth is calculated in Cuda time.
		//The program could obviously be written to not copy memory back and forth and instead torodize the grid in the GPU.
    //anything outside the scope of the GPU is NOT used for elapsed time calculation.
    for (i = 0; i < data->gens; i++){
        torodizeGrid(data);
        err = cudaMemcpy(cudaGrid, data->grid, size, cudaMemcpyHostToDevice);
        if (err) {
					printf("Error copying to gfx card: %s\n",cudaGetErrorString(err));
					cudaFree(cudaGrid);
					cudaFree(cudaNextGrid);
					return;
				}
        calculateCuda<<< gridSize, blockSize >>>(cudaGrid, cudaNextGrid, data->rows, data->cols);
        if (err = cudaGetLastError()){
					printf("Error executing kernel: %s\n",cudaGetErrorString(err));
				}
        // According to NVIDIA specs, the following function is blocking and aways for kernel to
        // to finish in order to execute memcpy. No further sync is required
        err = cudaMemcpy(data->nextGrid, cudaNextGrid, size, cudaMemcpyDeviceToHost);
        if (err) {
					printf("Error copying from gfx card: %s\n",cudaGetErrorString(err));
					cudaFree(cudaGrid);
					cudaFree(cudaNextGrid);
					return;
				}
        swap = data->grid;
        data->grid = data->nextGrid;
        data->nextGrid = swap;
    }
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&elapsedTime, start, stop);
    ///END CUDA
    file = fopen("times.log", "a");
    if (ftell(file) == 0L){
        fprintf(file, "GRID SIZE GENS CPU    CUDA\n");
    }
    fprintf(file, "%4dx%4d %4d %6.2f %.2f\n", data->rows, data->cols, data->gens, cpuElapsedTime, elapsedTime / 1000.0);
    fclose(file);
		cudaFree(cudaGrid);
		cudaFree(cudaNextGrid);
    return;
}
