#ifndef STRUCTS 
#define STRUCTS 

#define GRID(x,y) (*(data->grid + ((x) * (data->cols + 2)) + (y)))
#define NEXTGRID(x,y) (*(data->nextGrid + ((x) * (data->cols + 2)) + (y)))

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>
#include <cuda.h>

#define DEAD 0
#define ALIVE 1

typedef struct procInfo {
	FILE *in;
	int gens;
	int *grid;
	int *nextGrid;
	int rows;
	int cols;
	int bufSize;
} procInfo;

#endif
