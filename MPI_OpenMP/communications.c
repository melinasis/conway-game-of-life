#include "communications.h"

#define COLS (data->cols + 2)

void initiateSend(procInfo *data, MPI_Comm comm_graph) {
    MPI_Isend(&data->grid[1 * COLS + 1], 1, data->colType, data->left, TAG_EVAL_R, comm_graph, data->send[LEFT]);
    MPI_Isend(&data->grid[1 * COLS + data->cols], 1, data->colType, data->right, TAG_EVAL_L, comm_graph, data->send[RIGHT]);
    MPI_Isend(&data->grid[1 * COLS + 1], 1, data->rowType, data->up, TAG_EVAL_D, comm_graph, data->send[UP]);
    MPI_Isend(&data->grid[data->rows * COLS + 1], 1, data->rowType, data->down, TAG_EVAL_U, comm_graph, data->send[DOWN]);
    MPI_Isend(&data->grid[1 * COLS + 1], 1, MPI_INT, data->upLeft, TAG_EVAL_DR, comm_graph, data->send[UPLEFT]);
    MPI_Isend(&data->grid[1 * COLS + data->cols], 1, MPI_INT, data->upRight, TAG_EVAL_DL, comm_graph, data->send[UPRIGHT]);
    MPI_Isend(&data->grid[data->rows * COLS + 1], 1, MPI_INT, data->downLeft, TAG_EVAL_UR, comm_graph, data->send[DOWNLEFT]);
    MPI_Isend(&data->grid[data->rows * COLS + data->cols], 1, MPI_INT, data->downRight, TAG_EVAL_UL, comm_graph, data->send[DOWNRIGHT]);
    return;
}

void initiateReceive(procInfo *data, MPI_Comm comm_graph) {
    MPI_Irecv(&data->grid[1], 1, data->rowType, data->up, TAG_EVAL_U, comm_graph, data->receive[UP]);
    MPI_Irecv(&data->grid[(data->rows + 1) * COLS + 1], 1, data->rowType, data->down, TAG_EVAL_D, comm_graph, data->receive[DOWN]);
    MPI_Irecv(&data->grid[1 * COLS + data->cols + 1], 1, data->colType, data->right, TAG_EVAL_R, comm_graph, data->receive[RIGHT]);
    MPI_Irecv(&data->grid[1 * COLS], 1, data->colType, data->left, TAG_EVAL_L, comm_graph, data->receive[LEFT]);
    MPI_Irecv(&data->grid[(data->rows + 1) * COLS + data->cols + 1], 1, MPI_INT, data->downRight, TAG_EVAL_DR, comm_graph, data->receive[DOWNRIGHT]);
    MPI_Irecv(&data->grid[(data->rows + 1) * COLS], 1, MPI_INT, data->downLeft, TAG_EVAL_DL, comm_graph, data->receive[DOWNLEFT]);
    MPI_Irecv(&data->grid[data->cols + 1], 1, MPI_INT, data->upRight, TAG_EVAL_UR, comm_graph, data->receive[UPRIGHT]);
    MPI_Irecv(&data->grid[0], 1, MPI_INT, data->upLeft, TAG_EVAL_UL, comm_graph, data->receive[UPLEFT]);
}

void waitReceive(procInfo *data) {
    int i;
    for (i = 0; i < TOTAL_NEIGHBOURS; i++) {
        MPI_Wait(data->receive[i], MPI_STATUS_IGNORE);
    }
}

void waitSend(procInfo *data) {
    int i;
    for (i = 0; i < TOTAL_NEIGHBOURS; i++) {
        MPI_Wait(data->send[i], MPI_STATUS_IGNORE);
    }
}
