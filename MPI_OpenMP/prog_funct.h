#ifndef PROG_FUNCTIONS
#define PROG_FUNCTIONS

#include "structs.h"

void printHelp(char *progName);
int argHandling(execInfo *ret, int argc, char **argv);

#endif
