#ifndef COMMUNICATIONS
#define COMMUNICATIONS
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "structs.h"

void initiateSend(procInfo *data, MPI_Comm comm_graph);
void initiateReceive(procInfo *data, MPI_Comm comm_graph);
void waitReceive(procInfo *data);
void waitSend(procInfo *data);
#endif
