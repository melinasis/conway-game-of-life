#include "calculations.h"
#include <mpi.h>

#define COLS (data->cols + 2)

void calcExtern_firstColumn(procInfo *data) {
    int i, aliveNeighbours;
    for (i = 1; i <= data->rows; i++) {
        aliveNeighbours = data->grid[(i - 1) * COLS]
                + data->grid[i * COLS]
                + data->grid[(i + 1) * COLS]
                + data->grid[(i + 1) * COLS + 1]
                + data->grid[(i + 1) * COLS + 2]
                + data->grid[i * COLS + 2]
                + data->grid[(i - 1) * COLS + 2]
                + data->grid[(i - 1) * COLS + 1];
        if (data->grid[i * COLS + 1] == ALIVE) {
            switch (aliveNeighbours / ALIVE) {
                case 2:
                case 3:
                    data->nextGrid[i * COLS + 1] = ALIVE;
                    break;
                default: // 0 to 1 leads to loneliness
                    data->nextGrid[i * COLS + 1] = DEAD; // 4 to 8 leads to starvation
            }
        } else {
            if (aliveNeighbours / ALIVE == 3)
                data->nextGrid[i * COLS + 1] = ALIVE;
            else
                data->nextGrid[i * COLS + 1] = DEAD;
        }
        //printf("R(%d): (%d,%d):: AN:%d 1C\n",data->rank, i,1,aliveNeighbours);
    }
}

void calcExtern_lastColumn(procInfo *data) {
    int i, aliveNeighbours;
    for (i = 1; i <= data->rows; i++) {
        aliveNeighbours = data->grid[(i - 1) * COLS + data->cols - 1]
                + data->grid[i * COLS + data->cols - 1]
                + data->grid[(i + 1) * COLS + data->cols - 1]
                + data->grid[(i + 1) * COLS + data->cols]
                + data->grid[(i + 1) * COLS + data->cols + 1]
                + data->grid[i * COLS + data->cols + 1]
                + data->grid[(i - 1) * COLS + data->cols + 1]
                + data->grid[(i - 1) * COLS + data->cols];
        if (data->grid[i * COLS + data->cols] == ALIVE) {
            switch (aliveNeighbours / ALIVE) {
                case 2:
                case 3:
                    data->nextGrid[i * COLS + data->cols] = ALIVE;
                    break;
                default: // 0 to 1 - loneliness, 4 to 8 - starvation
                    data->nextGrid[i * COLS + data->cols] = DEAD;
            }
        } else {
            if (aliveNeighbours / ALIVE == 3)
                data->nextGrid[i * COLS + data->cols] = ALIVE;
            else
                data->nextGrid[i * COLS + data->cols] = DEAD;
        }
        //printf("R(%d): (%d,%d):: AN:%d FC\n",data->rank, i,data->rows,aliveNeighbours);
    }
}

void calcExtern_firstRow(procInfo *data) {
    int aliveNeighbours, j;
    for (j = 2; j < data->cols; j++) {
        aliveNeighbours = data->grid[j - 1]
                + data->grid[COLS + j - 1]
                + data->grid[2 * COLS + j - 1]
                + data->grid[2 * COLS + j]
                + data->grid[2 * COLS + j + 1]
                + data->grid[COLS + j + 1]
                + data->grid[j + 1]
                + data->grid[j];
        if (data->grid[COLS + j] == ALIVE) {
            switch (aliveNeighbours / ALIVE) {
                case 2:
                case 3:
                    data->nextGrid[COLS + j] = ALIVE;
                    break;
                default: // 0 to 1 - loneliness, 4 to 8 - starvation
                    data->nextGrid[COLS + j] = DEAD;
            }
        } else {
            if (aliveNeighbours / ALIVE == 3)
                data->nextGrid[COLS + j] = ALIVE;
            else
                data->nextGrid[COLS + j] = DEAD;
        }
        //printf("R(%d): (%d,%d):: AN:%d 1R\n",data->rank, 1,j,aliveNeighbours);
    }
}

void calcExtern_lastRow(procInfo *data) {
    int aliveNeighbours, j;
    for (j = 2; j < data->cols; j++) {
        aliveNeighbours = data->grid[(data->rows - 1) * COLS + j - 1]
                + data->grid[data->rows * COLS + j - 1]
                + data->grid[(data->rows + 1) * COLS + j - 1]
                + data->grid[(data->rows + 1) * COLS + j]
                + data->grid[(data->rows + 1) * COLS + j + 1]
                + data->grid[data->rows * COLS + j + 1]
                + data->grid[(data->rows - 1) * COLS + j + 1]
                + data->grid[(data->rows - 1) * COLS + j];
        if (data->grid[data->rows * COLS + j] == ALIVE) {
            switch (aliveNeighbours / ALIVE) {
                case 2:
                case 3:
                    data->nextGrid[data->rows * COLS + j] = ALIVE;
                    break;
                default: // 0 to 1 - loneliness, 4 to 8 - starvation
                    data->nextGrid[data->rows * COLS + j] = DEAD;
            }
        } else {
            if (aliveNeighbours / ALIVE == 3)
                data->nextGrid[data->rows * COLS + j] = ALIVE;
            else
                data->nextGrid[data->rows * COLS + j] = DEAD;
        }
        //printf("R(%d): (%d,%d):: AN:%d FR\n",data->rank, data->rows,j,aliveNeighbours);
    }
}

void calculateExternals(procInfo *data) {
    calcExtern_firstColumn(data); // First column
    calcExtern_firstRow(data); // First row, excluding first and last element which were calculated above
    calcExtern_lastColumn(data); // Last column
    calcExtern_lastRow(data); // Last row, excluding first and last element which were calculated above

}

void calculateInternals(procInfo *data) {
    int i, j, aliveNeighbours;
    for (i = 2; i < data->rows; i++) {
        for (j = 2; j < data->cols; j++) {
            aliveNeighbours = data->grid[(i - 1) * COLS + j - 1]
                    + data->grid[i * COLS + j - 1]
                    + data->grid[(i + 1) * COLS + j - 1]
                    + data->grid[(i + 1) * COLS + j]
                    + data->grid[(i + 1) * COLS + j + 1]
                    + data->grid[i * COLS + j + 1]
                    + data->grid[(i - 1) * COLS + j + 1]
                    + data->grid[(i - 1) * COLS + j];
            //printf("R(%d): (%d,%d):: AN:%d\n",data->rank, i,j,aliveNeighbours);
            if (data->grid[i * COLS + j] == ALIVE) {
                switch (aliveNeighbours / ALIVE) {
                    case 2:
                    case 3:
                        data->nextGrid[i * COLS + j] = ALIVE;
                        break;
                    default: // 0 to 1 - loneliness, 4 to 8 - starvation
                        data->nextGrid[i * COLS + j] = DEAD;
                }
            } else {
                if (aliveNeighbours / ALIVE == 3)
                    data->nextGrid[i * COLS + j] = ALIVE;
                else
                    data->nextGrid[i * COLS + j] = DEAD;
            }
        }
    }
}

MPI_Comm calcNeighbours(procInfo *process) {
    int procCols, procRows, myRank, totProcs;
    // 	int dest[TOTAL_NEIGHBOURS];
    // 	MPI_Comm ret;
    // 	int weights[TOTAL_NEIGHBOURS];
    // 	int sources,degrees;

    procRows = process->einfo.rows / process->rows;
    procCols = process->einfo.cols / process->cols;
    myRank = process->rank;
    totProcs = process->totalProcs;

    process->left = myRank - 1;
    process->right = myRank + 1;
    process->up = myRank - procCols;
    process->down = myRank + procCols;
    process->upRight = process->up + 1;
    process->upLeft = process->up - 1;
    process->downRight = process->down + 1;
    process->downLeft = process->down - 1;

    if (!(myRank % procCols)) { // Furthest to the left
        process->left += procCols;
        process->upLeft += procCols;
        process->downLeft += procCols;
    }

    if ((myRank % procCols) == procCols - 1) { // Furthest to the right
        process->right -= procCols;
        process->upRight -= procCols;
        process->downRight -= procCols;
    }

    if (!(myRank / procCols)) { // Furthest to the top
        process->up += totProcs;
        process->upRight += totProcs;
        process->upLeft += totProcs;
    }

    if ((myRank / procCols) == procRows - 1) { // Furthest to the bottom
        process->down -= totProcs;
        process->downLeft -= totProcs;
        process->downRight -= totProcs;
    }
    // Allocate as many MPI requests are required for each neighbour. The rest remain NULL (because of calloc)
    process->send = calloc(TOTAL_NEIGHBOURS, sizeof (MPI_Request *));
    process->receive = calloc(TOTAL_NEIGHBOURS, sizeof (MPI_Request *));

    process->send[LEFT] = malloc(sizeof (MPI_Request));
    process->receive[LEFT] = malloc(sizeof (MPI_Request));
    process->send[UPLEFT] = malloc(sizeof (MPI_Request));
    process->receive[UPLEFT] = malloc(sizeof (MPI_Request));
    process->send[UP] = malloc(sizeof (MPI_Request));
    process->receive[UP] = malloc(sizeof (MPI_Request));
    process->send[UPRIGHT] = malloc(sizeof (MPI_Request));
    process->receive[UPRIGHT] = malloc(sizeof (MPI_Request));
    process->send[RIGHT] = malloc(sizeof (MPI_Request));
    process->receive[RIGHT] = malloc(sizeof (MPI_Request));
    process->send[DOWNRIGHT] = malloc(sizeof (MPI_Request));
    process->receive[DOWNRIGHT] = malloc(sizeof (MPI_Request));
    process->send[DOWN] = malloc(sizeof (MPI_Request));
    process->receive[DOWN] = malloc(sizeof (MPI_Request));
    process->send[DOWNLEFT] = malloc(sizeof (MPI_Request));
    process->receive[DOWNLEFT] = malloc(sizeof (MPI_Request));

    //MPI_Cart

    return MPI_COMM_WORLD;
}

void calculateExternals_OpenMP(procInfo *data) {
    #pragma omp sections
    {
        #pragma omp section
        {
            calcExtern_firstColumn(data); // First column
        }
        #pragma omp section
        {
            calcExtern_firstRow(data); // First row, excluding first and last element which were calculated above
        }

        #pragma omp section
        {
            calcExtern_lastColumn(data); // Last column
        }
        #pragma omp section
        {
            calcExtern_lastRow(data); // Last row, excluding first and last element which were calculated above
        }
    }
}

void calculateInternals_OpenMP(procInfo *data) {
    int i, j, aliveNeighbours;

    // struct data can be private as there are no write collisions
    #pragma omp for private(i,j,aliveNeighbours) schedule(dynamic, 2) nowait
    for (i = 2; i < data->rows; i++) {
        for (j = 2; j < data->cols; j++) {
            aliveNeighbours = data->grid[(i - 1) * COLS + j - 1]
                    + data->grid[i * COLS + j - 1]
                    + data->grid[(i + 1) * COLS + j - 1]
                    + data->grid[(i + 1) * COLS + j]
                    + data->grid[(i + 1) * COLS + j + 1]
                    + data->grid[i * COLS + j + 1]
                    + data->grid[(i - 1) * COLS + j + 1]
                    + data->grid[(i - 1) * COLS + j];
            //printf("R(%d): (%d,%d):: AN:%d\n",data->rank, i,j,aliveNeighbours);
            if (data->grid[i * COLS + j] == ALIVE) {
                switch (aliveNeighbours / ALIVE) {
                    case 2:
                    case 3:
                        data->nextGrid[i * COLS + j] = ALIVE;
                        break;
                    default: // 0 to 1 - loneliness, 4 to 8 - starvation
                        data->nextGrid[i * COLS + j] = DEAD;
                }
            } else {
                if (aliveNeighbours / ALIVE == 3)
                    data->nextGrid[i * COLS + j] = ALIVE;
                else
                    data->nextGrid[i * COLS + j] = DEAD;
            }
        }
    }
}

void swapGrids(procInfo *data) {
    int *swap;
    swap = data->nextGrid;
    data->nextGrid = data->grid;
    data->grid = swap;
}

int equalGrids(procInfo *data) {
    int i, j;
    for (i = 1; i <= data->rows; i++) {
        for (j = 1; j <= data->cols; j++) {
            if (data->grid[i * COLS + j] != data->nextGrid[i * COLS + j])
                return 0;
        }
    }
    return 1;
}
