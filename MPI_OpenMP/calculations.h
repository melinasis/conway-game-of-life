#ifndef CALCULATIONS
#define CALCULATIONS
#include "structs.h"

MPI_Comm calcNeighbours(procInfo *process);

void calcExtern_firstColumn(procInfo *data);
void calcExtern_lastColumn(procInfo *data);
void calcExtern_firstRow(procInfo *data);
void calcExtern_lastRow(procInfo *data);

void calculateExternals(procInfo *data);
void calculateInternals(procInfo *data);

void calculateExternals_OpenMP(procInfo *data);
void calculateInternals_OpenMP(procInfo *data);

void swapGrids(procInfo *data);
int equalGrids(procInfo *data);

#endif
