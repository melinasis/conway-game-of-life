#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi_funct.h"
#include "structs.h"
#include "prog_funct.h"

int main(int argc, char *argv[]){
	execInfo *ein;
	procInfo process;
	int x, x1;
	double start,finish,elapsed,local_elapsed;

	FILE* times;
	ein = &process.einfo;


	/* MPI Initialization */
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &process.rank);
	MPI_Comm_size(MPI_COMM_WORLD, &process.totalProcs);

	if (argHandling(ein,argc,argv) == EXIT_FAILURE){
		MPI_Finalize();
		if(!process.rank)
			printHelp(argv[0]);
		return EXIT_FAILURE;
	}

	/* Calculate block dimensions */
	if ((ein->rows * ein->cols) % process.totalProcs){
		MPI_Finalize();
		if (process.rank == 0){
			printf("Cannot uniformly divide the matrix to processes\n");
		}
		return EXIT_FAILURE;
	}
	x = (ein->rows * ein->cols) / process.totalProcs;
	x1 = 1;

	process.rows = 0;
	process.cols = 0;
	while((x1 * x1) <= x){
		if (!(x % x1)){
			if ((!(ein->rows % x1)) && (!(ein->cols % (x / x1)))){
				process.rows = x1;
				process.cols = x / x1;
			}
			else if ((!(ein->cols % x1)) && (!(ein->rows % (x / x1)))){
				process.rows = x / x1;;
				process.cols = x1;
			}
		}
		x1++;
	}
	if (!process.rank)
		fprintf(stderr, "Process block dimensions have been fixed at: %d x %d\n", process.rows, process.cols);
	if (process.rows == 0){
		printf("Cannot match processes to matrix\n");
		MPI_Finalize();
		return EXIT_FAILURE;
	}
	process.sqpp = x;

	initGrid(&process);
	MPI_Barrier(MPI_COMM_WORLD);
	start = MPI_Wtime();
 	evaluateGrid(&process);
	finish = MPI_Wtime();
	local_elapsed = finish-start;
	MPI_Reduce(&local_elapsed,&elapsed,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);

	if(!process.rank){
		times = fopen("mpi_times.log","a");
		fprintf(times,"%s\t%s\t%d\t%dx%d\t%d\t%.2lf\n",(ein->calcP == OPENMP) ? ((ein->numThreads == 2) ? "OpenMP2" : "OpenMP4") :"MPI",
						(ein->extd > 0) ? "GLOBCOMM":"NOGLOBCO", process.totalProcs,ein->rows,ein->cols,ein->gens,elapsed);
	}

	destroy(&process);
	MPI_Finalize();
	return EXIT_SUCCESS;
}
