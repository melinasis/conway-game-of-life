#include "prog_funct.h"

#define COLS (block->cols + 2)

void printHelp(char *progName) {
    printf("Usage: %s [argument (value)]\n\
\033[22;33m-N\033[m [number]                  number of rows\n\
\033[22;33m-M\033[m [number]                  number of columns\n\
\033[22;33m--generate\033[m [number]    / \033[22;33m-g\033[m  [number] amount of generations to iterate through\n\
\033[22;33m--file\033[m [filename]      / \033[22;33m-f\033[m  [filename] grid & live organisms location input file(optional)\n\
\033[22;33m--global-comm\033[m [number] / \033[22;33m-x\033[m  global communication mode every [number] generations (optional)\n\
\033[22;33m--openMP\033[m [number]      / \033[22;33m-o\033[m  [number] use openMP with [number == 2 or 4] threads for calculations(optional)\n", progName);
    // \033[22;33m--cuda\033[m                 / \033[22;33m-d\033[m  use cuda for calculations(optional)\n
}

int argHandling(execInfo *ret, int argc, char **argv) {
    // Variable declarations
    int argIter;
    // Variable initializations
    argIter = 1;
    ret->cols = ret->rows = ret->gens = ret->extd = ret->calcP = 0;
    ret->in = NULL;
    // Argument handling
    while (argIter < argc) {
        if (argv[argIter][0] == '-') {
            if (argv[argIter][2] == '\0') {
                switch (argv[argIter][1]) {
                    case 'N':
                        if (++argIter == argc)
                            break;
                        ret->rows = strtol(argv[argIter], NULL, 10);
                        break;
                    case 'M':
                        if (++argIter == argc)
                            break;
                        ret->cols = strtol(argv[argIter], NULL, 10);
                        break;
                    case 'f':
                        if (++argIter == argc)
                            break;
                        ret->in = fopen(argv[argIter], "r");
                        if (ret->in == NULL) {
                            perror(argv[argIter]);
                        }
                        break;
                    case 'g':
                        if (++argIter == argc)
                            break;
                        ret->gens = strtol(argv[argIter], NULL, 10);
                        break;
                    case 'x':
                        if (++argIter == argc)
                            return EXIT_FAILURE;
                        ret->extd = strtol(argv[argIter], NULL, 10);
                        if (ret->extd < 1)
                            return EXIT_FAILURE;
                        break;
                    case 'o':
                        if (++argIter == argc)
                            return EXIT_FAILURE;
                        ret->calcP = OPENMP;
                        ret->numThreads = strtol(argv[argIter], NULL, 10);
                        if ((ret->numThreads != 2) && (ret->numThreads != 4))
                            return EXIT_FAILURE;
                        break;
                        //                  case 'd':
                        //                      ret->calcP = CUDA;
                        //                      break;
                    default:
                        return EXIT_FAILURE;
                }
            } else if (argv[argIter][1] == '-') {
                if (!strcmp(&argv[argIter][2], "global-comm")) {
                    if (++argIter == argc)
                        return EXIT_FAILURE;
                    ret->extd = strtol(argv[argIter], NULL, 10);
                    if (ret->extd < 1)
                        return EXIT_FAILURE;
                } else if (!strcmp(&argv[argIter][2], "file")) {
                    if (++argIter == argc)
                        return EXIT_FAILURE;
                    ret->in = fopen(argv[argIter], "r");
                    if (ret->in == NULL) {
                        return EXIT_FAILURE;
                    }
                } else if (!strcmp(&argv[argIter][2], "generations")) {
                    if (++argIter == argc)
                        break;
                    ret->gens = strtol(argv[argIter], NULL, 10);
                } else if (!strcmp(&argv[argIter][2], "openMP")) {
                    if (++argIter == argc)
                        return EXIT_FAILURE;
                    ret->calcP = OPENMP;
                    ret->numThreads = strtol(argv[argIter], NULL, 10);
                    if ((ret->numThreads != 2) && (ret->numThreads != 4))
                        return EXIT_FAILURE;
                }
                //         else if (!strcmp(&argv[argIter][2], "cuda")){
                //           ret->calcP = CUDA;
                //         }
            } else {
                return EXIT_FAILURE;
            }
        } else {
            return EXIT_FAILURE;
        }
        ++argIter;
    }
    if ((!ret->rows) || (!ret->cols) || (!ret->gens)) {
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
