#ifndef STRUCTS
#define STRUCTS

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <mpi.h>
#include <omp.h>

#define DEAD 0
#define ALIVE 1

//#define CUDA     0x123
#define OPENMP 0x124

#define LEFT 0
#define RIGHT 1
#define UP 2
#define DOWN 3
#define UPRIGHT 4
#define UPLEFT 5
#define DOWNRIGHT 6
#define DOWNLEFT 7

#define TOTAL_NEIGHBOURS 8
#define SEND 0x0
#define RECV 0x2
#define COLL 0x0
#define COLR 0x1

#define TAG_PRINT 31
#define TAG_EVAL_U 32
#define TAG_EVAL_D 33
#define TAG_EVAL_L 34
#define TAG_EVAL_R 35
#define TAG_EVAL_UL 36
#define TAG_EVAL_UR 37
#define TAG_EVAL_DL 38
#define TAG_EVAL_DR 39


typedef struct execInfo {
  FILE *in;  //inputFile
  int gens;  //generations
  int extd;  //extended mode (global communication)
  int rows;  //amount of rows
  int cols;  //amount of columns
	// int printStdO; //print grid to standard output (1: normal, 2: colored process blocks)
	int calcP; //calculation parallelization: NONE / CUDA / OPENMP
	int numThreads;
} execInfo;

typedef struct procInfo {
  //*Neighbours*//
	int up;
	int down;
	int left;
	int right;
	int upLeft;
	int upRight;
	int downLeft;
	int downRight;
	//*Data*//
	int rank;
	int totalProcs;
	int *grid;
	int *nextGrid;
	int rows;
	int cols;
	int sqpp;	 //squares per process
	int amtN;
	int bufSize;
	execInfo einfo;
	MPI_Request **send;
	MPI_Request **receive;
	MPI_Datatype rowType;
	MPI_Datatype colType;
} procInfo;

#endif
