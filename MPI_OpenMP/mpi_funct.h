#ifndef MPI_FUNCTIONS
#define MPI_FUNCTIONS

#include "structs.h"
#include "prog_funct.h"

void loadFile(procInfo *process);
void destroy(procInfo *process);
void randomizeGrid(procInfo *process);
void initGrid(procInfo *process);
void evaluateGrid(procInfo *process);
#endif
