#include "mpi_funct.h"
#include "communications.h"
#include "calculations.h"
#include "structs.h"


#define COLS (data->cols + 2)

void loadFile(procInfo *data) {
    int i, j, rowMin, rowMax, colMin, colMax, procCols;
    procCols = data->einfo.cols / data->cols;
    rowMin = (data->rank / procCols) * data->rows;
    colMin = (data->rank % procCols) * data->cols;
    rowMax = rowMin + (data->rows - 1);
    colMax = colMin + (data->cols - 1);
    while (fscanf(data->einfo.in, "%d %d\n", &i, &j) != EOF) {
        if ((rowMin <= i) && (i <= rowMax)) {
            if ((colMin <= j) && (j <= colMax)) {
                i = i % data->rows + 1; // +1 because grid beings from (1,1)
                j = j % data->cols + 1;
                data->grid[i * COLS + j] = ALIVE;
            }
        }
    }
}

void destroy(procInfo *block) {
    free(block->grid);
    free(block->nextGrid);
}

void randomizeGrid(procInfo *block) {
    int i, j;

    srand(time(NULL) / (block->rank + 1));
    for (i = 1; i <= block->rows; i++)
        for (j = 1; j <= block->cols; j++)
            block->grid[i * (block->cols + 2) + j] = (rand() % 2) ? ALIVE : DEAD;
}

void initGrid(procInfo *block) {

    MPI_Type_vector(block->cols, 1, 1, MPI_INT, &block->rowType);
    MPI_Type_commit(&block->rowType);
    MPI_Type_vector(block->rows, 1, block->cols + 2, MPI_INT, &block->colType);
    MPI_Type_commit(&block->colType);

    block->grid = calloc((block->rows + 2) * (block->cols + 2), sizeof (int*));

    block->nextGrid = calloc((block->rows + 2) * (block->cols + 2), sizeof (int*));

    if (block->einfo.in == NULL) {
        randomizeGrid(block);
    } else {
        loadFile(block);
    }
}

void evaluateGrid(procInfo *data) {
    int i, j, k, commCount;
    MPI_Comm comm_graph;

    comm_graph = calcNeighbours(data);

    //The following part (excluding calculations) is linear. Choosing to initialize the parallel method here leads to a single point
    //at which threads begin, thus minimizing the overhead compared to the case of beginning threads at each time they are required
#pragma omp parallel if ((data->rows > 1) && (data->cols > 1) && (data->einfo.calcP)) num_threads(data->einfo.numThreads) private(i, commCount)
    {
        commCount = 0;
        for (i = 0; i < data->einfo.gens; i++) {
            #pragma omp master
            {
                initiateSend(data, comm_graph);
                initiateReceive(data, comm_graph);
            }
            if (data->einfo.calcP == OPENMP) { //OpenMP functionality
            #pragma omp barrier
                calculateInternals_OpenMP(data);
            #pragma omp master
                {
                    waitReceive(data);
                }
            #pragma omp barrier
                calculateExternals_OpenMP(data);
            #pragma omp master
                {
                    waitSend(data);
                }
            } else { // Only MPI functionality
                calculateInternals(data);
                waitReceive(data);
                calculateExternals(data);
                waitSend(data);
            }
            if (data->einfo.extd && (++commCount == data->einfo.extd)) { //j, k used as temps in this context
                commCount = 0;
                if (equalGrids(data)) {
                    j = 0;
                } else { // Grids not equal
                    j = 1;
                }
                MPI_Allreduce(&j, &k, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
                if (!k) { // No processes changed data, halt evaluation
                    break;
                }
            }
            #pragma omp master
            {
                swapGrids(data);
            }
        }
    }
    return;
}
